
{
    let items = [1, 2, 3, null];
    const map1 = new Map();
    items.forEach((i)=> {
        const value = typeof i;
        map1.set(i, value);
    });

    console.table(map1);
}

{
    let items = [0, 1, null, 'Hi'];
    const map1 = new Map();
    items.forEach((i)=> {
        const value = typeof i;
        map1.set(i, value);
    });

    console.table(map1);
}

{
    let arr = [new Date(), new RegExp('\d+')];
    const map1 = new Map();
    arr.forEach((i)=> {
        const value = typeof i;
        map1.set(i, value);
    });

    console.table(map1);
}