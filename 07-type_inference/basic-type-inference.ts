{
    let counter: number;
    console.log(counter = 0);
}

{
    let counter = 0;

    function increment(counter: number) {
        return counter++;
    }    

    console.log(counter);

    counter = increment(99);
    console.log(counter);
}

{
    let counter: number = 0;
    console.log(counter);

    function setCounter(max=100) {
        counter = max;
    }

    function increment1(counter: number) : number {
        return counter++;
    }

    setCounter();
    console.log(counter);

    setCounter(9999);
    console.log(counter);

    counter = increment1(32332);
    console.log(counter);
}