/*
let variableName: type;
let variableName: type = value;
const constantName: type = value;
*/

function test1() {
    let counter: number;
    counter = 1;
    console.log(counter);
}

function test2() {
    let counter: number;
    //counter = 'Hello'; // compile error 
    //console.log(counter);
}

function test3() {
    let counter: number = 1;
    console.log(counter);
}

test1();
test2();
test3();

{
    let name: string = 'John';
    let age: number = 25;
    let active: boolean = true;
    console.log(name, age, active);
}

let names: string[] = ['John', 'Jane', 'Peter', 'David', 'Mary'];
console.table(names);

let person: {
    name: string;
    age: number
};

person = {
    name: 'John',
    age: 25
}; // valid
console.log(person);

let greeting : (name: string) => string;
greeting = function (name: string) {
    return `Hi ${name}`;
};
console.log(greeting("federico"));

/*greeting = function () {
    console.log('Hello');
};*/
