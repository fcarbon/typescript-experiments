import alert from 'alert';
// Tagged Union Types for modelling state that can be in one of many shapes
type State =
    | { type: "loading" }
    | { type: "success", value: number }
    | { type: "error", message: string };

declare const state: State;

function learn_x_in_y_minutes() {

    {
        // There are 3 basic types in TypeScript
        let isDone: boolean = false;
        let lines: number = 42;
        let name: string = "Anders";
        let members = [[isDone, typeof (isDone)],
        [lines, typeof (lines)],
        [name, typeof (name)]];
        console.table(members);
    }

    {
        // But you can omit the type annotation if the variables are derived
        // from explicit literals
        let isDone = false;
        let lines = 42;
        let name = "Anders";
        let members = [[isDone, typeof (isDone)],
        [lines, typeof (lines)],
        [name, typeof (name)]];
        console.table(members);
    }

    {
        // When it's impossible to know, there is the "Any" type
        let notSures = [];
        let notSure: any = 4;
        notSures.push([notSure, typeof (notSure)]);
        notSure = "maybe a string instead";
        notSures.push([notSure, typeof (notSure)]);
        notSure = false; // okay, definitely a boolean
        notSures.push([notSure, typeof (notSure)]);
        console.table(notSures);
    }

    {
        // Use const keyword for constants
        const numLivesForCat = 9;
        console.log(numLivesForCat);
        //numLivesForCat = 1; // Error
    }

    {
        // For collections, there are typed arrays and generic arrays
        let list: number[] = [1, 2, 3];
        console.table(list);
        // Alternatively, using the generic array type
        let list1: Array<number> = [1, 2, 3];
        console.table(list1);
    }

    {
        // For enumerations:
        enum Color { Red, Green, Blue };
        let c: Color = Color.Green;
        console.log(Color[c]); // "Green"
    }

    {
        // Lastly, "void" is used in the special case of a function returning nothing
        function bigHorribleAlert(): void {
            alert("I'm a little annoying box!");
        }
        bigHorribleAlert();
    }

    {
        // Functions are first class citizens, support the lambda "fat arrow" syntax and
        // use type inference

        // The following are equivalent, the same signature will be inferred by the
        // compiler, and same JavaScript will be emitted
        let list = [];
        let result = Math.PI;
        let f1 = function (i: number): number { return i * i; }
        list.push([result, result = f1(result)]);
        // Return type inferred
        let f2 = function (i: number) { return i * i; }
        list.push([result, result = f2(result)]);
        // "Fat arrow" syntax
        let f3 = (i: number): number => { return i * i; }
        list.push([result, result = f3(result)]);
        // "Fat arrow" syntax with return type inferred
        let f4 = (i: number) => { return i * i; }
        list.push([result, result = f4(result)]);
        // "Fat arrow" syntax with return type inferred, braceless means no return
        // keyword needed
        let f5 = (i: number) => i * i;
        list.push([result, result = f5(result)]);
        console.table(list);
    }

    {
        // Interfaces are structural, anything that has the properties is compliant with
        // the interface
        interface Person {
            name: string;
            // Optional properties, marked with a "?"
            age?: number;
            // And of course functions
            move(): void;
        }

        let persons = [];
        // Object that implements the "Person" interface
        // Can be treated as a Person since it has the name and move properties
        let p: Person = { name: "Bobby", move: () => { } };
        persons.push(p);
        // Objects that have the optional property:
        let validPerson: Person = { name: "Bobby", age: 42, move: () => { } };
        persons.push(validPerson);
        // Is not a person because age is not a number
        //let invalidPerson: Person = { name: "Bobby", age: true };
        console.table(persons);
    }

    {
        // Interfaces can also describe a function type
        interface SearchFunc {
            (source: string, subString: string): boolean;
        }

        // Only the parameters' types are important, names are not important.
        let mySearch: SearchFunc;
        mySearch = function (src: string, sub: string) {
            return src.search(sub) != -1;
        }
        const searches = [];
        let string1 = "To be or not to be...";
        let string2 = "Let it be...";
        let string3 = "To be a good person...";
        searches.push([string1, "To be", mySearch(string1, "To be")]);
        searches.push([string2, "it", mySearch(string2, "it")]);
        searches.push([string3, "what", mySearch(string3, "what")]);
        console.table(searches);
    }

    {
        // Classes - members are public by default
        class Point {
            // Properties
            x: number;

            // Constructor - the public/private keywords in this context will generate
            // the boiler plate code for the property and the initialization in the
            // constructor.
            // In this example, "y" will be defined just like "x" is, but with less code
            // Default values are also supported

            constructor(x: number, public y: number = 0) {
                this.x = x;
            }

            // Functions
            dist(): number { return Math.sqrt(this.x * this.x + this.y * this.y); }

            // Static members
            static origin = new Point(0, 0);
        }

        interface Person {
            name: string;
            // Optional properties, marked with a "?"
            age?: number;
            // And of course functions
            move(): void;
        }

        // Classes can be explicitly marked as implementing an interface.
        // Any missing properties will then cause an error at compile-time.
        class PointPerson implements Person {
            constructor(name: string) {
                this.name = name;
            }
            name: string;
            move() { }
        }

        let p1 = new Point(10, 20);
        let p2 = new Point(25); //y will be 0
        const points = [];
        points.push(p1);
        points.push(p2);
        points.push(new PointPerson("hello"));
        console.table(points);

        // Inheritance
        class Point3D extends Point {
            constructor(x: number, y: number, public z: number = 0) {
                super(x, y); // Explicit call to the super class constructor is mandatory
            }

            // Overwrite
            dist(): number {
                let d = super.dist();
                return Math.sqrt(d * d + this.z * this.z);
            }
        }

        let pd1 = new Point3D(2, 4, 5);
        console.log(pd1.dist());

        // Generics
        // Classes
        class Tuple<T1, T2> {
            constructor(public item1: T1, public item2: T2) {
            }
        }

        // Interfaces
        interface Pair<T> {
            item1: T;
            item2: T;
        }

        // And functions
        let pairToTuple = function <T>(p: Pair<T>) {
            return new Tuple(p.item1, p.item2);
        };

        let tuple = pairToTuple({ item1: "hello", item2: "world" });
        console.log(tuple.item1, tuple.item2);
    }

    {
        // Including references to a definition file:
        /// <reference path="jquery.d.ts" />

        // Template Strings (strings that use backticks)
        // String Interpolation with Template Strings
        let name = 'Tyrone';
        let greeting = `Hi ${name}, how are you?`
        console.log(greeting);
        // Multiline Strings with Template Strings
        let multiline = `This is an example
of a multiline string`;
        console.log(multiline);

        // READONLY: New Feature in TypeScript 3.1
        interface Person {
            readonly name: string;
            readonly age: number;
        }

        var p1: Person = { name: "Tyrone", age: 42 };
        //p1.age = 25; // Error, p1.age is read-only
        console.log(p1);

        var p2 = { name: "John", age: 60 };
        console.log(p2);
        var p3: Person = p2; // Ok, read-only alias for p2
        console.log(p3);
        //p3.age = 35; // Error, p3.age is read-only
        p2.age = 45; // Ok, but also changes p3.age because of aliasing
        console.log(p1, p2, p3);

        class Car {
            readonly make: string;
            readonly model: string;
            readonly year = 2018;

            constructor() {
                this.make = "Unknown Make"; // Assignment permitted in constructor
                this.model = "Unknown Model"; // Assignment permitted in constructor
            }
        }

        const car = new Car();
        console.log(car);

        let numbers: Array<number> = [0, 1, 2, 3, 4];
        let moreNumbers: ReadonlyArray<number> = numbers;
        //moreNumbers[5] = 5; // Error, elements are read-only
        //moreNumbers.push(5); // Error, no push method (because it mutates array)
        //moreNumbers.length = 3; // Error, length is read-only
        //numbers = moreNumbers; // Error, mutating methods are missing

        const state = { type: "success", value: 202, message: "Success" };

        if (state.type === "success") {
            console.log(state.value);
        } else if (state.type === "error") {
            console.error(state.message);
        }

        // Template Literal Types
        // Use to create complex string types
        type OrderSize = "regular" | "large";
        type OrderItem = "Espresso" | "Cappuccino";
        type Order = `A ${OrderSize} ${OrderItem}`;

        let order1: Order = "A regular Cappuccino";
        let order2: Order = "A large Espresso";
        //let order3: Order = "A small Espresso"; // Error
        console.log(order1, order2);

        // Iterators and Generators

        // for..of statement
        // iterate over the list of values on the object being iterated
        let arrayOfAnyType = [1, "string", false];
        for (const val of arrayOfAnyType) {
            console.log(val); // 1, "string", false
        }

        let list = [4, 5, 6];
        for (const i of list) {
            console.log(i); // 4, 5, 6
        }

        // for..in statement
        // iterate over the list of keys on the object being iterated
        for (const i in list) {
            console.log(i); // 0, 1, 2
        }

        // Type Assertion
        {
            let foo = {} // Creating foo as an empty object
            //foo.bar = 123 // Error: property 'bar' does not exist on `{}`
            //foo.baz = 'hello world' // Error: property 'baz' does not exist on `{}`
        }

        // Because the inferred type of foo is `{}` (an object with 0 properties), you 
        // are not allowed to add bar and baz to it. However with type assertion,
        // the following will pass:

        interface Foo {
            bar: number;
            baz: string;
        }

        let foo = {} as Foo; // Type assertion here
        foo.bar = 123;
        foo.baz = 'hello world'
        console.log(foo);

    }
}

learn_x_in_y_minutes();


